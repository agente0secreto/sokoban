﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraDesigner : MonoBehaviour
{
    private Camera camera;
    
    public Transform puntMoviment;
    public float velocitat;
    
    // Start is called before the first frame update
    void Start()
    {
        camera = GetComponent<Camera>();
        puntMoviment.parent = null;
    }

    // Update is called once per frame
    void Update()
    {
        
        transform.position = Vector3.MoveTowards(transform.position, puntMoviment.position, velocitat * Time.deltaTime);
        
        if (Vector3.Distance(transform.position, puntMoviment.position) <= 0.05f)
        {
            //CAMERA
            Vector3 direction = Vector3.zero;

            if (Math.Abs(Input.GetAxisRaw(Constants.AXIS_HORIZONTAL)) == 1f)
            {
                direction = new Vector3(Input.GetAxisRaw(Constants.AXIS_HORIZONTAL), 0f, 0f);
            }
            else if (Math.Abs(Input.GetAxisRaw(Constants.AXIS_VERTICAL)) == 1f)
            {
                direction = new Vector3(0f, Input.GetAxisRaw(Constants.AXIS_VERTICAL), 0f);
            }

            if (direction != Vector3.zero)
            {
                puntMoviment.position = camera.transform.position + direction;
            }
        }
    }

    public void Move(Vector3 position)
    {
        puntMoviment.position = position;
        camera.transform.position = position;
    }
}
