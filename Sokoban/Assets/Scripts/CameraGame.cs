﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraGame : MonoBehaviour
{
    
    private Transform player;
    private bool hasPlayer = false;

    /// <summary>
    /// Calcula la variable offset y la aplica tras setear la variable player
    /// </summary>
    public void SetPlayer(Transform player)
    {
        this.player = player;
        transform.position = GetPosition();
        hasPlayer = true;
    }
    
    private void Update()
    {
        if (hasPlayer)
        {
            transform.position = GetPosition(); 
        }
    }

    private Vector3 GetPosition()
    {
        return new Vector3(player.position.x, player.position.y,transform.position.z);
    }
}
