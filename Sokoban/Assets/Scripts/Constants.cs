﻿public class Constants
{
    public static readonly string TAG_PLAYER = "Player";
    public static readonly string TAG_BOX = "Box";

    public static readonly string AXIS_HORIZONTAL = "Horizontal";
    public static readonly string AXIS_VERTICAL = "Vertical";

    public static readonly string PREFS_WIDTH = "Width";
    public static readonly string PREFS_HEIGHT = "Height";
    public static readonly string PREFS_SELECTED_LEVEL = "SelectedLvl";

    public static readonly string SCENE_DESIGNER = "DesignerMode";
    public static readonly string SCENE_HOME = "Home";
    public static readonly string SCENE_LEVEL_SELECTOR = "LevelSelector";
    public static readonly string SCENE_GAME = "Game";
}