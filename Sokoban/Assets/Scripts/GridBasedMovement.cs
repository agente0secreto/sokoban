﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridBasedMovement : MonoBehaviour
{
    public float velocitat = 0.5f;
    public Transform puntMoviment;

    [HideInInspector]
    public Tilemap Tilemap;


    public void SetTilemap(Tilemap tilemap)
    {
        Tilemap = tilemap;
        puntMoviment.parent = null;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = Vector3.MoveTowards(transform.position, puntMoviment.position, velocitat * Time.deltaTime);

        if (Vector3.Distance(transform.position, puntMoviment.position) <= 0.05f)
        {

            Vector3 direction = Vector3.zero;
            
            if (Math.Abs(Input.GetAxisRaw(Constants.AXIS_HORIZONTAL)) == 1f)
            {
                direction = new Vector3(Input.GetAxisRaw(Constants.AXIS_HORIZONTAL), 0f, 0f);
            }else if (Math.Abs(Input.GetAxisRaw(Constants.AXIS_VERTICAL)) == 1f)
            {
                direction = new Vector3(0f, Input.GetAxisRaw(Constants.AXIS_VERTICAL), 0f);
            }

            if (direction != Vector3.zero)
            {
                Tilemap.TilemapObject tilemapObject = Tilemap.GetTileObject(puntMoviment.position + direction);
                
                if(tilemapObject.GetOnTile() == Tilemap.TilemapObject.OnTile.BOX) 
                {
                    ObjectMovement objectMovement = tilemapObject.GetOnTileObject().GetComponent<ObjectMovement>();

                    if (objectMovement.CanMove(direction))
                    {
                        objectMovement.Move(direction);
                    }
                }
                else if (tilemapObject.GetTilemapSprite() != Tilemap.TilemapObject.TilemapSprite.WALL)
                {
                    puntMoviment.position += direction;
                }
            }
        }
        
    }
}
