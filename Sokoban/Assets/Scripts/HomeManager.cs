﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class HomeManager : MonoBehaviour
{
    public GameObject DesignModal;
    public InputField Width;
    public InputField Height;
    
    private List<Image> loadItems;
    private string selectedLoadItem;

    public void ClickPlay()
    {
        SceneManager.LoadScene(Constants.SCENE_LEVEL_SELECTOR);
    }

    public void ClickDesign()
    {
        if (validateNumber(Width.text.Trim()) && validateNumber(Height.text.Trim()))
        {
            
            PlayerPrefs.SetInt(Constants.PREFS_WIDTH, int.Parse(Width.text.Trim()));
            PlayerPrefs.SetInt(Constants.PREFS_HEIGHT, int.Parse(Width.text.Trim()));

            SceneManager.LoadScene(Constants.SCENE_DESIGNER);
        }
        else
        {
            Debug.Log("Por favor, introduce solo numeros en los campos correspondientes");
        }
    }

    public void ClickDesignCancel()
    {
        DesignModal.SetActive(false);
    }

    public void ClickDesignShowModal()
    {
        DesignModal.SetActive(true);
    }

    private bool validateNumber(string number)
    {
        return int.TryParse(number, out _);
    }
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
