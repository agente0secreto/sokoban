﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelSelectorManager : MonoBehaviour
{

    public HorizontalLayoutGroup layout;
    public Button prefabButtonLevel;

    public void ClickExit()
    {
        SceneManager.LoadScene(Constants.SCENE_HOME);
    }
    
    private void OnClick(Button sender)
    {
        string selectedLevel = sender.GetComponentInChildren<Text>().text;
        
        PlayerPrefs.SetString(Constants.PREFS_SELECTED_LEVEL, selectedLevel);
        SceneManager.LoadScene(Constants.SCENE_GAME);
    }
    
    
    void Start()
    {
        List<string> files = SaveSystem.LoadDirectoryContent();

        foreach (string file in files)
        {
            Button image = Instantiate(prefabButtonLevel,layout.transform);
            Utils.AddListener(image.gameObject, arg0 => OnClick(image));
            image.GetComponentInChildren<Text>().text = file;

        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
