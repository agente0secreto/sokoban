﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectMovement : MonoBehaviour
{
    private SpriteRenderer spriteRenderer;
    public event EventHandler OnButtonTilePressed;
    public event EventHandler OnButtonTileExit;
    
    public float velocitat = 0.5f;
    public Transform puntMoviment;

    public bool canMove;
    private bool isMoving;

    [HideInInspector] public Tilemap Tilemap;
    private Tilemap.TilemapObject thisTilemapObject;
    private Tilemap.TilemapObject nextTilemapObject;

    
    public void SetTilemap(Tilemap tilemap)
    {
        Tilemap = tilemap;
        puntMoviment.parent = null;
    }

    public void SetThisTilemapObject(Tilemap.TilemapObject thisTilemapObject)
    {
        this.thisTilemapObject = thisTilemapObject;
    }

    private void Start()
    {
        spriteRenderer = GetComponentInChildren<SpriteRenderer>();
        OnButtonTilePressed += ButtonPressed_ChangeColor;
        OnButtonTileExit += ButtonExit_ChangeColor;
    }


    // Update is called once per frame
    void Update()
    {
        if (isMoving)
        {
            if (Vector3.Distance(transform.position, puntMoviment.position) <= 0.05f)
            {
                thisTilemapObject.SetOntile(Tilemap.TilemapObject.OnTile.NONE);
                thisTilemapObject.SetOnTileObject(null);
                nextTilemapObject.SetOntile(Tilemap.TilemapObject.OnTile.BOX);
                nextTilemapObject.SetOnTileObject(this);
                

                if (nextTilemapObject.GetTilemapSprite() == Tilemap.TilemapObject.TilemapSprite.BUTTON &&
                    thisTilemapObject.GetTilemapSprite() != Tilemap.TilemapObject.TilemapSprite.BUTTON)
                {
                    OnButtonTilePressed?.Invoke(this, EventArgs.Empty);
                } else if (nextTilemapObject.GetTilemapSprite() != Tilemap.TilemapObject.TilemapSprite.BUTTON &&
                           thisTilemapObject.GetTilemapSprite() == Tilemap.TilemapObject.TilemapSprite.BUTTON)
                {
                    OnButtonTileExit?.Invoke(this,EventArgs.Empty);
                }

                thisTilemapObject = nextTilemapObject;
                isMoving = false;
            }
        }
        
        transform.position = Vector3.MoveTowards(transform.position, puntMoviment.position, velocitat * Time.deltaTime);
    }


    public bool CanMove(Vector3 direction)
    {
        Tilemap.TilemapObject tilemapObject = Tilemap.GetTileObject(puntMoviment.position + direction);

        return canMove && !isMoving && tilemapObject.GetTilemapSprite() != Tilemap.TilemapObject.TilemapSprite.BOX
               && tilemapObject.GetTilemapSprite() != Tilemap.TilemapObject.TilemapSprite.WALL
               && tilemapObject.GetTilemapSprite() != Tilemap.TilemapObject.TilemapSprite.NONE;
    }

    public void Move(Vector3 direction)
    {
        var position = puntMoviment.position;
        nextTilemapObject = Tilemap.GetTileObject(position + direction);

        puntMoviment.transform.position = position + direction;
        isMoving = true;

    }

    private void ChangeColor(Color color)
    {
        spriteRenderer.color = color;
    }
    
    private void ButtonPressed_ChangeColor(object sender, EventArgs e)
    {
        ChangeColor(Color.green);
    }

    public void ButtonExit_ChangeColor(object sender, EventArgs e)
    {
        ChangeColor(Color.white);
    }

}
