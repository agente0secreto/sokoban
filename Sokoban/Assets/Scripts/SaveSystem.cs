﻿/* 
    ------------------- Code Monkey -------------------

    Thank you for downloading this package
    I hope you find it useful in your projects
    If you have any questions let me know
    Cheers!

               unitycodemonkey.com
    --------------------------------------------------
 */

using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class SaveSystem {

    private const string SAVE_EXTENSION = "json";

    private static readonly string SAVE_FOLDER = Application.dataPath + "/Saves/";
    private static bool isInit = false;

    public static void Init() {
        if (!isInit) {
            isInit = true;
            // Test if Save Folder exists
            if (!Directory.Exists(SAVE_FOLDER)) {
                // Create Save Folder
                Directory.CreateDirectory(SAVE_FOLDER);
            }
        }
    }

    public static void Save(string fileName, string saveString, bool overwrite) {
        Init();
        string saveFileName = fileName;
        if (!overwrite) {
            // Make sure the Save Number is unique so it doesnt overwrite a previous save file
            //No añade el contenido al archivo, sino que crea uno nuevo con un numero
            int saveNumber = 1;
            while (File.Exists(SAVE_FOLDER + saveFileName + "." + SAVE_EXTENSION)) {
                saveNumber++;
                saveFileName = fileName + "_" + saveNumber;
            }
        }
        File.WriteAllText(SAVE_FOLDER + saveFileName + "." + SAVE_EXTENSION, saveString);
    }

    public static List<string> LoadDirectoryContent()
    {
        List<string> loadFiles = new List<string>();
        foreach (var enumerateFile in Directory.GetFiles(SAVE_FOLDER))
        {
            if (enumerateFile.Contains("." + SAVE_EXTENSION))
            {
                if (!enumerateFile.Contains(".meta"))
                {
                    string file = Path.GetFileName(enumerateFile).Replace("." + SAVE_EXTENSION, "");
                    loadFiles.Add(file);
                }
            }
        }

        return loadFiles;
    }
    public static string Load(string fileName) {
        Init();
        if (File.Exists(SAVE_FOLDER + fileName + "." + SAVE_EXTENSION)) {
            string saveString = File.ReadAllText(SAVE_FOLDER + fileName + "." + SAVE_EXTENSION);
            return saveString;
        } else {
            return null;
        }
    }

    public static string LoadMostRecentFile() {
        Init();
        DirectoryInfo directoryInfo = new DirectoryInfo(SAVE_FOLDER);
        // Get all save files
        FileInfo[] saveFiles = directoryInfo.GetFiles("*." + SAVE_EXTENSION);
        // Cycle through all save files and identify the most recent one
        FileInfo mostRecentFile = null;
        foreach (FileInfo fileInfo in saveFiles) {
            if (mostRecentFile == null) {
                mostRecentFile = fileInfo;
            } else {
                if (fileInfo.LastWriteTime > mostRecentFile.LastWriteTime) {
                    mostRecentFile = fileInfo;
                }
            }
        }

        // If theres a save file, load it, if not return null
        if (mostRecentFile != null) {
            string saveString = File.ReadAllText(mostRecentFile.FullName);
            return saveString;
        } else {
            return null;
        }
    }

    public static void SaveObject(object saveObject) {
        SaveObject("save", saveObject, false);
    }

    public static void SaveObject(string fileName, object saveObject, bool overwrite) {
        Init();
        string json = JsonUtility.ToJson(saveObject);
        Save(fileName, json, overwrite);
    }

    public static TSaveObject LoadMostRecentObject<TSaveObject>() {
        Init();
        string saveString = LoadMostRecentFile();
        if (saveString != null) {
            TSaveObject saveObject = JsonUtility.FromJson<TSaveObject>(saveString);
            return saveObject;
        } else {
            return default(TSaveObject);
        }
    }

    public static TSaveObject LoadObject<TSaveObject>(string fileName) {
        Init();
        string saveString = Load(fileName);
        if (saveString != null) {
            TSaveObject saveObject = JsonUtility.FromJson<TSaveObject>(saveString);
            return saveObject;
        } else {
            return default(TSaveObject);
        }
    }

}
