﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tilemap
{
    public event EventHandler OnLoaded;
    public Grid<TilemapObject> grid;

    public Tilemap(int width, int height, float cellSize, Vector3 originPosition)
    {
        grid = new Grid<TilemapObject>(width,height,cellSize,originPosition,
            (Grid<TilemapObject> g, int x, int y) => new TilemapObject(g, x, y));
    }

    public Tilemap(SaveObject saveObject, Vector3 originPosition)
    {
        grid = new Grid<TilemapObject>(saveObject.width,saveObject.height,saveObject.cellSize, originPosition, 
            (Grid<TilemapObject> g, int x, int y) => new TilemapObject(g, x, y));
        
        Load(saveObject);
    }

    public TilemapObject GetTileObject(Vector3 worldPosition)
    {
        return grid.GetGridObject(worldPosition);
    }

    public TilemapObject GetTileObjectWithType(TilemapObject.TilemapSprite type)
    {
        for (int x = 0; x < grid.GetWidth(); x++)
        {
            for (int y = 0; y < grid.GetHeight(); y++)
            {
                TilemapObject tilemapObject = grid.GetGridObject(x, y);

                if (tilemapObject.GetTilemapSprite() == type)
                {
                    return tilemapObject;
                }
            }
        }
        
        throw new Exception("No se encontró ningún objeto con este tipo");
    }

    public List<TilemapObject> GetTileObjectsWithType(TilemapObject.TilemapSprite type)
    {
        List<TilemapObject> objects = new List<TilemapObject>();
        
        for (int x = 0; x < grid.GetWidth(); x++)
        {
            for (int y = 0; y < grid.GetHeight(); y++)
            {
                TilemapObject tilemapObject = grid.GetGridObject(x, y);

                if (tilemapObject.GetTilemapSprite() == type)
                {
                    objects.Add(tilemapObject);
                }
            }
        }

        if (objects.Count == 0)
        {
            throw new Exception("No se encontró ningún objeto con este tipo");
        }

        return objects;
    }

    public void SetTilemapSprite(Vector3 worldPosition, TilemapObject.TilemapSprite tilemapSprite)
    {
        TilemapObject tilemapObject = grid.GetGridObject(worldPosition);
        if (tilemapObject != null)
        {
            tilemapObject.SetTilemapSprite(tilemapSprite);
        }
    }
    
    public void SetTilemapVisual(TilemapVisual tilemapVisual)
    {
        tilemapVisual.SetGrid(this, grid);
    }

    public void Save(string fileName)
    {
        List<TilemapObject.SaveObject> tilemapObjectSaveObjectList = new List<TilemapObject.SaveObject>();
        for (int x = 0; x < grid.GetWidth(); x++)
        {
            for (int y = 0; y < grid.GetHeight(); y++)
            {
                TilemapObject tilemapObject = grid.GetGridObject(x, y);
                tilemapObjectSaveObjectList.Add(tilemapObject.Save());
            }
        }

        SaveObject saveObject = new SaveObject
        {
            width = grid.GetWidth(),
            height = grid.GetHeight(),
            cellSize = grid.GetCellSize(),
            tilemapObjectSaveArray = tilemapObjectSaveObjectList.ToArray()
        };
        
        SaveSystem.SaveObject(fileName, saveObject,true);
    }

    public void Load()
    {
        SaveObject saveObject = SaveSystem.LoadMostRecentObject<SaveObject>();
        foreach (TilemapObject.SaveObject tilemapObjectSave in saveObject.tilemapObjectSaveArray)
        {
            TilemapObject tilemapObject = grid.GetGridObject(tilemapObjectSave.x, tilemapObjectSave.y);
            tilemapObject.Load(tilemapObjectSave);
        }
        OnLoaded?.Invoke(this,EventArgs.Empty);
    }

    public void Load(SaveObject saveObject)
    {
        foreach (TilemapObject.SaveObject tilemapObjectSave in saveObject.tilemapObjectSaveArray)
        {
            TilemapObject tilemapObject = grid.GetGridObject(tilemapObjectSave.x, tilemapObjectSave.y);
            tilemapObject.Load(tilemapObjectSave);
        }
        OnLoaded?.Invoke(this,EventArgs.Empty);
    }

    public class SaveObject
    {
        public int width;
        public int height;
        public float cellSize;
        public TilemapObject.SaveObject[] tilemapObjectSaveArray;
    }
    public class TilemapObject
    {
        private Grid<TilemapObject> grid;
        public int x;
        public int y;

        private TilemapSprite tilemapSprite;
        private OnTile onTile;
        private ObjectMovement onTileObject;

        public enum TilemapSprite
        {
            NONE, GROUND, WALL, BOX, BUTTON, PLAYER
        }

        public enum OnTile
        {
            NONE, BOX
        }
        
        public TilemapObject(Grid<TilemapObject> grid, int x, int y)
        {
            this.grid = grid;
            this.x = x;
            this.y = y;
            this.onTile = OnTile.NONE;
        }

        public void SetTilemapSprite(TilemapSprite tilemapSprite)
        {
            this.tilemapSprite = tilemapSprite;
            grid.TriggerGridObjectChanged(x,y);
        }

        public TilemapSprite GetTilemapSprite()
        {
            return tilemapSprite;
        }

        public void SetOntile(OnTile onTile)
        {
            this.onTile = onTile;
        }

        public OnTile GetOnTile()
        {
            return onTile;
        }

        public void SetOnTileObject(ObjectMovement OnTileObject)
        {
            onTileObject = OnTileObject;
        }

        public ObjectMovement GetOnTileObject()
        {
            return onTileObject;
        }

        public override string ToString()
        {
            return tilemapSprite.ToString();
        }

        public SaveObject Save()
        {
            return new SaveObject
            {
                TilemapSprite = tilemapSprite,
                OnTile = onTile,
                x = x,
                y = y
            };
        }

        public void Load(SaveObject saveObject)
        {
            tilemapSprite = saveObject.TilemapSprite;
            onTile = saveObject.OnTile;
        }
        
        [Serializable]
        public class SaveObject
        {
            public TilemapSprite TilemapSprite;
            public OnTile OnTile;
            public int x;
            public int y;
        }
    }

}
