﻿using System;
using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class TilemapDesigner : MonoBehaviour
{
    // Start is called before the first frame update
    private Tilemap tilemap;
    [SerializeField] private TilemapVisual tilemapVisual;
    private Tilemap.TilemapObject.TilemapSprite actualTilemapSprite;

    public GameObject LoadModal;
    public GameObject SaveModal;
    public InputField SaveName;
    public VerticalLayoutGroup VerticalLayoutGroup;

    public Image loadItemPrefab;
    private string selectedLoadItem;
    private List<Image> loadItems;

    public int width;
    public int height;
    
    void Start()
    {
        tilemap = new Tilemap(PlayerPrefs.GetInt(Constants.PREFS_WIDTH),PlayerPrefs.GetInt(Constants.PREFS_HEIGHT),
            1, new Vector3(-3,-2));
        tilemap.SetTilemapVisual(tilemapVisual);
        actualTilemapSprite = Tilemap.TilemapObject.TilemapSprite.NONE;

        Vector3 cameraPosition = tilemap.grid.GetWorldPosition(width / 2, height / 2);
        cameraPosition.z = -10;
        Camera.main.GetComponent<CameraDesigner>().Move(cameraPosition);
        
        loadItems = new List<Image>();
    }

    // Update is called once per frame
    void Update()
    {
        //DESIGNER
        if (Input.GetMouseButtonDown(0))
        {
            // Check if the mouse was clicked over a UI element
            if (!EventSystem.current.IsPointerOverGameObject())
            {
                tilemap.SetTilemapSprite(Utils.GetMouseWorldPosition(), actualTilemapSprite);
            }
        }
    }

    public void ClickDelete()
    {
        SetActualTilemapSprite(Tilemap.TilemapObject.TilemapSprite.NONE);
    }

    public void ClickWall()
    {
        SetActualTilemapSprite(Tilemap.TilemapObject.TilemapSprite.WALL);
    }

    public void ClickGround()
    {
        SetActualTilemapSprite(Tilemap.TilemapObject.TilemapSprite.GROUND);
    }

    public void ClickBox()
    {
        SetActualTilemapSprite(Tilemap.TilemapObject.TilemapSprite.BOX);
    }

    public void ClickButton()
    {
        SetActualTilemapSprite(Tilemap.TilemapObject.TilemapSprite.BUTTON);
    }

    public void ClickPlayer()
    {
        SetActualTilemapSprite(Tilemap.TilemapObject.TilemapSprite.PLAYER);
    }

    public void ClickSave()
    {
        tilemap.Save(SaveName.text);
        SaveModal.SetActive(false);
    }

    public void ClickSaveShowModal()
    {
        SaveModal.SetActive(true);
    }

    public void ClickSaveCancel()
    {
        SaveModal.SetActive(false);
    }
    
    public void ClickLoad()
    {
        if (selectedLoadItem != null)
        {
            Tilemap.SaveObject saveObject = SaveSystem.LoadObject<Tilemap.SaveObject>(selectedLoadItem);
            Vector3 originPosition = tilemap.grid.originPosition;
            tilemap = new Tilemap(saveObject, originPosition);
            tilemap.SetTilemapVisual(tilemapVisual);
            
            ClickLoadCancel();
        }
        
    }
    
    public void ClickLoadShowModal()
    {
        
        List<string> files = SaveSystem.LoadDirectoryContent();

        foreach (string file in files)
        {
            Image image = Instantiate(loadItemPrefab,VerticalLayoutGroup.transform);
            loadItems.Add(image);
            Utils.AddListener(image.gameObject, arg0 => OnSelectedLoadItem(image));

            image.GetComponentInChildren<Text>().text = file;

        }
        
        LoadModal.SetActive(true);
    }

    public void ClickLoadCancel()
    {
        LoadModal.SetActive(false);

        foreach (var loadItem in loadItems)
        {
            Destroy(loadItem.gameObject);
        }
        
        loadItems = new List<Image>();
    }

    public void ClickExit()
    {
        SceneManager.LoadScene(Constants.SCENE_HOME);
    }

    public void OnSelectedLoadItem(Image button)
    {
        UnselectAllLoadItems();
        
        button.color = Color.blue;
        Text text = button.GetComponentInChildren<Text>();
        text.color = Color.white;

        selectedLoadItem = text.text;
    }

    private void UnselectAllLoadItems()
    {
        foreach (var item in loadItems)
        {
            if (item.color == Color.blue)
            {
                item.color = Color.clear;
                Text text = item.GetComponentInChildren<Text>();
                text.color = Color.black;
            }
        }
    }
    
    private void SetActualTilemapSprite(Tilemap.TilemapObject.TilemapSprite tilemapSprite)
    {
        actualTilemapSprite = tilemapSprite;
        Debug.Log(actualTilemapSprite.ToString());
    }
}
