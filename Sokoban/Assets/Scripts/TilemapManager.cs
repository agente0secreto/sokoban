﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TilemapManager : MonoBehaviour
{
    // Start is called before the first frame update
    private Tilemap tilemap;
    [SerializeField] private TilemapVisual tilemapVisual;
    private List<Tilemap.TilemapObject> buttons;
    
    public GridBasedMovement player;
    public ObjectMovement boxPrefab;

    
    void Start()
    {

        Tilemap.SaveObject saveObject = SaveSystem.LoadObject<Tilemap.SaveObject>(PlayerPrefs.GetString(Constants.PREFS_SELECTED_LEVEL));
        
        tilemap = new Tilemap(saveObject, transform.position);
        tilemap.SetTilemapVisual(tilemapVisual);
        
        //INSTANCIO EL JUGADOR
        Tilemap.TilemapObject playerCell = tilemap.GetTileObjectWithType(Tilemap.TilemapObject.TilemapSprite.PLAYER);
        
        Vector3 playerPosition = tilemap.grid.GetWorldPosition(playerCell.x, playerCell.y) + new Vector3(tilemap.grid.GetCellSize(), tilemap.grid.GetCellSize()) * 0.5f;
        player = Instantiate(player, playerPosition, transform.rotation);
        playerCell.SetTilemapSprite(Tilemap.TilemapObject.TilemapSprite.GROUND);
        player.SetTilemap(tilemap);
        
        //INSTANCIO LOS BOTONES
        buttons = tilemap.GetTileObjectsWithType(Tilemap.TilemapObject.TilemapSprite.BUTTON);
        
        //INSTANCIO LAS CAJAS
        List<Tilemap.TilemapObject> cajas = tilemap.GetTileObjectsWithType(Tilemap.TilemapObject.TilemapSprite.BOX);

        foreach (Tilemap.TilemapObject caja in cajas)
        {
            Vector3 boxPosition = tilemap.grid.GetWorldPosition(caja.x, caja.y) + new Vector3(tilemap.grid.GetCellSize(), tilemap.grid.GetCellSize()) * 0.5f;
            ObjectMovement cajaGO = Instantiate(boxPrefab, boxPosition, transform.rotation);
            caja.SetTilemapSprite(Tilemap.TilemapObject.TilemapSprite.GROUND);
            caja.SetOntile(Tilemap.TilemapObject.OnTile.BOX);
            caja.SetOnTileObject(cajaGO);
            cajaGO.SetTilemap(tilemap);
            cajaGO.SetThisTilemapObject(caja);
            cajaGO.OnButtonTilePressed += CheckIfLevelIsCompleted;
        }
        
        Camera.main.GetComponent<CameraGame>().SetPlayer(player.transform);
    }

    private void CheckIfLevelIsCompleted(object sender, EventArgs e)
    {
        if (CheckIfAllButtonsPressed())
        {
            //TODO: hacer sonar musiquita de enhorabuena
            ClickExit();
        }
    }

    private bool CheckIfAllButtonsPressed()
    {
        foreach (Tilemap.TilemapObject button in buttons)
        {
            if (button.GetOnTile() == Tilemap.TilemapObject.OnTile.NONE)
            {
                return false;
            }
        }

        return true;
    }

    public void ClickExit()
    {
        SceneManager.LoadScene(Constants.SCENE_LEVEL_SELECTOR);
    }

    public void ClickReload()
    {
        SceneManager.LoadScene(Constants.SCENE_GAME);
    }
    
}
