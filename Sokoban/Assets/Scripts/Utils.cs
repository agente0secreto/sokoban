﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class Utils
{
    //WORLD TEXT GENERATOR
    public class WorldTextArgs
    {
        public Transform Parent = null;
        public Vector3 LocalPosition = new Vector3();
        public int FontSize = 20;
        public TextAnchor TextAnchor = TextAnchor.MiddleCenter;
        public Color Color = Color.white;
        public string Text = "";
        public int SortingOrder = 0;
        public TextAlignment TextAlignment = TextAlignment.Center;
        public float CharacterSize = 0.1f;
    }
    
    public static TextMesh CreateWorldText(WorldTextArgs args)
    {
            GameObject gameObject = new GameObject("World_Text", typeof(TextMesh));
            Transform transform = gameObject.transform;
            transform.SetParent(args.Parent,false);
            transform.localPosition = args.LocalPosition;
            TextMesh textMesh = gameObject.GetComponent<TextMesh>();
            textMesh.anchor = args.TextAnchor;
            textMesh.alignment = args.TextAlignment;
            textMesh.text = args.Text;
            textMesh.fontSize = args.FontSize;
            textMesh.color = args.Color;
            textMesh.characterSize = args.CharacterSize;
            textMesh.GetComponent<MeshRenderer>().sortingOrder = args.SortingOrder;
            return textMesh;
    }

    //MOUSE POSITION GETTER
    public static Vector3 GetMouseWorldPosition()
    {
        return GetMouseWorldPosition(Camera.main);
    }
    
    public static Vector3 GetMouseWorldPosition(Camera worldCamera)
    {
        Vector3 vec = GetMouseWorldPositionWithZ(Input.mousePosition, worldCamera);
        vec.z = 0f;
        return vec;
    }

    public static Vector3 GetMouseWorldPositionWithZ()
    {
        return GetMouseWorldPositionWithZ(Input.mousePosition, Camera.main);
    }
    
    public static Vector3 GetMouseWorldPositionWithZ(Camera worldCamera)
    {
        return GetMouseWorldPositionWithZ(Input.mousePosition, worldCamera);
    }
    
    public static Vector3 GetMouseWorldPositionWithZ(Vector3 screenPosition, Camera worldCamera)
    {
        Vector3 worldPosition = worldCamera.ScreenToWorldPoint(screenPosition);
        return worldPosition;
    }
    
    /// <summary>
    /// Añade un listener al gameObject
    /// </summary>
    /// <param name="gameObject">La opción que va a contener el listener, debe contener el componente EventTrigger</param>
    /// <param name="insulto">El insulto asociado a esta opción, visualmente puede ser tanto un ataque como una respuesta</param>
    /// <param name="tipo">El tipo del jugador que ha empezado el asalto</param>
    public static void AddListener(GameObject gameObject, UnityAction<BaseEventData> call)
    {
        EventTrigger evento = gameObject.GetComponent<EventTrigger>();
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.PointerClick;
        entry.callback.AddListener(call);
        evento.triggers.Add(entry);
    }

    
}