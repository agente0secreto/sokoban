# PEC 3 - Sokoban - Felip Lluís Yankovich Llopis

## Introducción

Bienvenido a mi tercera práctica de Diseño de Niveles, en esta ocasión he desarrollado una versión de Sokoban

## ¿Cómo Jugar?

Para jugar basta con darle al botón play en la scene Home, luego elegir la opción Jugar y por último seleccionar el nivel deseado. Cada nivel se completa cuando se consigue pulsar todos los botones con las cajas, para ello habrá que empujarlas a lo largo del nivel

**Controles:**

**Flechas de dirección:** Mover al personaje

## ¿Cómo Diseñar?

Para diseñar, hay que elegir la opción "Diseñar un nivel" en la escena Home. Tras esto, se abrirá una ventana modal que pedirá el ancho y largo del nivel en números enteros. Después, se abrirá el editor. Basta con pulsar el sprite que queremos dibujar y clicar sobre la posición que deseamos. Una vez esté creado nuestro nivel, clicaremos sobre guardar y le pondremos el nombre que queramos.


**Controles:**

**Flechas de dirección:** Mover la cámara
**Click izquierdo del ratón:** Pintar un sprite en una celda determinada

## Demo

Diseñador: https://drive.google.com/file/d/1cdCTM0gBu8r-lSXozlOlCBXI5vFZhp8Z/view?usp=sharing
Juego: https://drive.google.com/file/d/13QVJ2oBdwrvVMMtOsulxzSgCWQBF8mkU/view?usp=sharing
